package pe.uni.mhuamanir.pc3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

public class CalculatorMenuActivity extends AppCompatActivity {

    RadioButton basicButton, scientificButton, programerButton;
    Button nextButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator_menu);

        basicButton=findViewById(R.id.basic_calculator);
        scientificButton=findViewById(R.id.scientific_calculator);
        programerButton=findViewById(R.id.programer_calculator);
        nextButton=findViewById(R.id.next);

        nextButton.setOnClickListener(v -> {

            Intent intent=new Intent(CalculatorMenuActivity.this,BasicCalclatorActivity.class);

            if(programerButton.isChecked()||scientificButton.isChecked()){
                Toast.makeText(getApplicationContext(),R.string.construction_text,Toast.LENGTH_LONG).show();
                return;
            }
            if(basicButton.isChecked()){
                startActivity(intent);
            }

        });


    }
}