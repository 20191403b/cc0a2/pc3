package pe.uni.mhuamanir.pc3;

import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressLint("CustomSplashScreen")
public class MainActivity extends AppCompatActivity {

    ImageView imageView;
    TextView tittle;
    Animation animationImage,animationText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imageView=findViewById(R.id.image_menu);
        tittle=findViewById(R.id.text_menu);

        animationImage=AnimationUtils.loadAnimation(this,R.anim.image_animation);
        animationText=AnimationUtils.loadAnimation(this,R.anim.text_animation);

        imageView.setAnimation(animationImage);
        tittle.setAnimation(animationText);
        new CountDownTimer(6000,1000){
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                Intent intent =new Intent(MainActivity.this,CalculatorMenuActivity.class);
                startActivity(intent);
                finish();

            }
        }.start();

    }
}